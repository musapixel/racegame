package com.come.racegame.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.come.racegame.model.Car;

public class GameScreen implements Screen{

	private TextureAtlas textureAtlas;
	private SpriteBatch batch;
	private Car car;
	private OrthographicCamera camera;
	
	public static float deltaCff;
	
	@Override
	public void show() {
		batch = new SpriteBatch();
		car = new Car(textureAtlas.findRegion("car2"), 0, 0, 1f, 1f * textureAtlas.findRegion("car2").getRegionHeight() / textureAtlas.findRegion("car2").getRegionWidth());
		
	}
	
	public void setTextureAtlas(TextureAtlas textureAtlas) {
		this.textureAtlas = textureAtlas;
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		deltaCff = delta;
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		car.draw(batch);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		float aspectRatio = (float) height / width;
		camera = new OrthographicCamera(20f, 20f * aspectRatio);
		camera.zoom = 0.6f;
		camera.update();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		textureAtlas.dispose();
		batch.dispose();
	}
	
}
