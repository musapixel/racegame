package com.come.racegame.model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.come.racegame.control.CarController;

public class Car extends GameObject {
	
	private CarController carController;
	public Car(TextureRegion textureRegion, float x, float y, float width, float height) {
		super(textureRegion, x, y, width, height);
		carController = new CarController(bounds);
	}
	@Override
	public void draw(SpriteBatch batch) {
		super.draw(batch);
		carController.handle();
	}
}